# Example of using common Gitlab pipeline definition among different branches

This is an example showing how a repo with multiple branches can maintain a commmon pipeline .gitlab-ci.yml 

This technique has unique variable files for each branch, so that "git merge" continues to work properly without collisions when you want to update common files.

* Included variables are loaded from 'envs/<branch>.yml'
* Application variables are loaded from 'appvars/<branch>.properties'
* Gitlab runner tags set by variable

This allows common files like .gitlab-ci.yml or terraform files to be tested on the 'main' branch, then higher level branches can do a simple "git merge main" to get the updates.


## Create MR on new branch

```
cur_branch=$(git branch --show-current)
new_branch="new-branch-from-${cur_branch}-$RANDOM"
git checkout -b $new_branch
git commit -a -m "MR request targeted for $cur_branch"
git push -u origin $new_branch
```

## Delete MR branch after successful merge

```
git branch -d $new_branch; git push -d origin $new_branch
```
